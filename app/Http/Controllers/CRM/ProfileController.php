<?php

namespace App\Http\Controllers\CRM;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function saveImage(Request $request)
    {
        return response()->json($request->all());
    }
}
